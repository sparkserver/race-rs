//! Describes packets that are sent from the server to a client

use super::SyncBits;
use byteorder::{WriteBytesExt, BE};
use std::io::Write;

#[derive(Debug)]
pub struct SlotBits(u8);

impl SlotBits {
    pub fn from_slot_count(count: u8) -> Self {
        let bits = 0xFF >> (8 - count);
        Self(bits)
    }

    pub fn as_u8(&self) -> u8 {
        self.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn slot_bits_from_count() {
        let tests = &[(1, 0b1), (2, 0b11), (3, 0b111), (4, 0b1111), (5, 0b11111)];
        for (count, value) in tests {
            assert_eq!(SlotBits::from_slot_count(*count).as_u8(), *value);
        }
    }
}

/// Sync packet sent from the client to the server
#[derive(Debug)]
pub struct ServerSync {
    pub counter: u16,
    /// Current client time
    pub time: u16,
    /// Client hello time
    pub client_hello_time: u16,
    /// Counter of the last packet that the client received from the server
    pub client_counter: u16,
    pub sync_bits: SyncBits,
    pub subpacket: SyncSubpacket,
}

impl ServerSync {
    pub fn write_to<W: Write>(&self, wr: &mut W) {
        wr.write_u8(0x00);
        wr.write_u16::<BE>(self.counter);
        wr.write_u8(0x02);
        wr.write_u16::<BE>(self.time);
        wr.write_u16::<BE>(self.client_hello_time);
        wr.write_u16::<BE>(self.client_counter);
        wr.write_u16::<BE>(self.sync_bits.0);
        self.subpacket.write_to(wr);
        wr.write_u32::<BE>(0x55555555);
    }
}

#[derive(Debug)]
pub struct StartInfo {
    pub grid_idx: u8,
    pub session_id: u32,
    pub slots: SlotBits,
}

#[derive(Debug)]
pub enum SyncSubpacket {
    /// Just a keep-alive packet
    None,
    Start(StartInfo),
    Sync(u16),
}

impl SyncSubpacket {
    pub fn write_to<W: Write>(&self, wr: &mut W) {
        let (ty, len) = match self {
            Self::None => (0xFF, 0),
            Self::Start(..) => (0x00, 6),
            Self::Sync(..) => (0x01, 3),
        };
        wr.write_u8(ty);
        if ty == 0xFF {
            return;
        }

        wr.write_u8(len);
        match self {
            Self::Start(s) => {
                wr.write_u8(s.grid_idx);
                wr.write_u32::<BE>(s.session_id);
                wr.write_u8(s.slots.0);
            }
            Self::Sync(s) => {
                wr.write_u8(0x00);
                wr.write_u16::<BE>(*s);
            }
            _ => unreachable!(),
        }

        wr.write_u8(0xFF);
    }
}
