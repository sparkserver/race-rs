//! Describes packets that are sent from a client to the server

use super::SyncBits;

/// Sync packet sent from the client to the server
#[derive(Debug)]
pub struct ClientSync {
    pub counter: u16,
    /// Current client time
    pub time: u16,
    /// Client hello time
    pub hello_time: u16,
    /// Counter of the last packet that the client received from the server
    pub server_counter: u16,
    pub sync_bits: SyncBits,
    pub subpacket: ClientSyncSubpacket,
}

impl ClientSync {
    pub fn parse(buf: &[u8]) -> Self {
        let counter = u16::from_be_bytes([buf[1], buf[2]]);
        let time = u16::from_be_bytes([buf[5], buf[6]]);
        let hello_time = u16::from_be_bytes([buf[7], buf[8]]);
        let server_counter = u16::from_be_bytes([buf[9], buf[10]]);
        let sync_bits = u16::from_be_bytes([buf[11], buf[12]]);
        let subpacket = ClientSyncSubpacket::parse(&buf[13..buf.len() - 4]);
        Self {
            counter,
            time,
            hello_time,
            server_counter,
            sync_bits: SyncBits(sync_bits),
            subpacket,
        }
    }
}

#[derive(Debug)]
pub enum ClientSyncSubpacket {
    /// Just a keep-alive packet
    None,
    Start {
        session_id: u32,
        slot: u8,
        max_players: u8,
    },
    Sync(u16),
}

impl ClientSyncSubpacket {
    pub fn parse(buf: &[u8]) -> Self {
        if let Some(Subpacket { kind, data }) = parse_subpacket(buf) {
            match kind {
                0x00 => Self::parse_start(data),
                0x01 => Self::parse_sync(data),
                _ => panic!("Unknown subpacket"),
            }
        } else {
            Self::None
        }
    }

    fn parse_start(buf: &[u8]) -> Self {
        let session_id = u32::from_be_bytes([buf[1], buf[2], buf[3], buf[4]]);
        let slot = buf[5];
        Self::Start {
            session_id,
            slot: slot >> 4,
            max_players: (slot & 0xF) >> 1,
        }
    }

    fn parse_sync(buf: &[u8]) -> Self {
        let value = u16::from_be_bytes([buf[0], buf[1]]);
        Self::Sync(value)
    }
}

fn parse_subpacket(buf: &[u8]) -> Option<Subpacket<'_>> {
    Subpackets::from_bytes(buf).next()
}

pub struct Subpacket<'a> {
    pub kind: u8,
    pub data: &'a [u8],
}

pub struct Subpackets<'a> {
    offset: usize,
    buffer: &'a [u8],
}

impl<'a> Subpackets<'a> {
    pub fn from_bytes(buffer: &'a [u8]) -> Self {
        Self { offset: 0, buffer }
    }
}

impl<'a> Iterator for Subpackets<'a> {
    type Item = Subpacket<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let kind = self.buffer[self.offset];
        if kind == 0xFF {
            None
        } else {
            let len = self.buffer[self.offset + 1] as usize;
            let buf = &self.buffer[self.offset + 1..self.offset + len + 1];
            self.offset += len + 1;
            Some(Subpacket { kind, data: buf })
        }
    }
}
