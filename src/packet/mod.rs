//! Definitions for network packets

pub mod clientbound;
pub mod serverbound;

use std::fmt::{self, Debug, Display};

pub struct SyncBits(u16);

impl SyncBits {
    pub fn from_count(count: u16) -> Self {
        if count == 0 || count > 16 {
            Self(0xFFFF)
        } else {
            let bits = 1 << (16 - count);
            Self(!bits)
        }
    }

    pub fn as_u16(&self) -> u16 {
        self.0
    }
}

impl Display for SyncBits {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:016b}", self.0)
    }
}

impl Debug for SyncBits {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SyncBits({:016b})", self.0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sync_bits_from_count() {
        let tests = &[
            (0, 0b11111111_11111111),
            (1, 0b01111111_11111111),
            (2, 0b10111111_11111111),
            (9, 0b11111111_01111111),
            (16, 0b11111111_11111110),
            (17, 0b11111111_11111111),
            (1234, 0b11111111_11111111),
        ];
        for (count, value) in tests {
            let sb = SyncBits::from_count(*count);
            assert_eq!(
                sb.as_u16(),
                *value,
                "count {} results in {:?} not {:016b}",
                *count,
                sb,
                *value
            );
        }
    }
}
