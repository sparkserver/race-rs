use byteorder::{WriteBytesExt, BE};
use packet::{
    clientbound::{self, SlotBits, SyncSubpacket},
    serverbound::ClientSyncSubpacket,
    SyncBits,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    io::{Cursor, Write},
    net::{SocketAddr, ToSocketAddrs, UdpSocket},
    time::Instant,
};

pub mod packet;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

pub struct GlobalClock {
    start: Instant,
}

impl GlobalClock {
    pub fn new() -> Self {
        Self {
            start: Instant::now(),
        }
    }

    pub fn elapsed_ms_u16(&self) -> u16 {
        let millis = self.start.elapsed().as_millis();
        (millis & 0xFFFF) as u16
    }
}

pub struct SessionManager {
    pub sessions: RefCell<HashMap<u32, Session>>,
}

impl SessionManager {
    pub fn new() -> Self {
        Self {
            sessions: RefCell::new(HashMap::new()),
        }
    }
}

pub struct Session {
    pub id: u32,
    pub slots: Vec<Option<SocketAddr>>,
}

#[derive(PartialEq, Eq)]
pub enum ClientState {
    /// Client has handshaked with the server
    Connected,
    /// Client has sent at least one Sync+Start packet and is assigned to a session
    InSession,
}

pub struct Client<'a> {
    /// State of the client
    pub state: ClientState,
    /// Remote address of the client, also used as unique identifier for the client
    pub addr: SocketAddr,
    /// Client timestamp at the time of the hello packet
    pub cli_time: u16,
    /// Counter that gets incremented with every outbound hello/sync packet
    pub sync_counter: u16,
    /// Counter that gets incremented with every outbound data packet
    pub data_counter: u16,

    //weak_ref: Weak<RefCell<Client<'static>>>,
    global_clock: &'a GlobalClock,
    sessions: &'a SessionManager,
    /// Buffer for composing outbound packets
    packet_buf: Vec<u8>,
}

type PacketWrite<'a> = Cursor<&'a mut Vec<u8>>;

impl<'a> Client<'a> {
    fn new(
        global_clock: &'a GlobalClock,
        sessions: &'a SessionManager,
        addr: SocketAddr,
        cli_time: u16,
    ) -> Self {
        Self {
            global_clock,
            sessions,
            state: ClientState::Connected,
            addr,
            cli_time,
            sync_counter: 0,
            data_counter: 0,
            packet_buf: Vec::with_capacity(4096),
        }
    }

    fn respond_hello(&mut self, socket: &UdpSocket) -> Result<()> {
        let srv_time = self.global_clock.elapsed_ms_u16();
        let cli_time = self.cli_time;
        self.send_packet(socket, |wr| {
            wr.write_all(&[0x00, 0x00, 0x00, 0x01]);
            wr.write_u16::<BE>(srv_time);
            wr.write_u16::<BE>(cli_time);
            wr.write_all(&[0x55, 0x55, 0x55, 0x55]);
        });
        self.sync_counter += 1;
        Ok(())
    }

    fn process_sync_start(&mut self, id: u32, _slot: u8) {
        if self.state != ClientState::Connected {
            return;
        }
        self.state = ClientState::InSession;

        let mut map = self.sessions.sessions.borrow_mut();
        if let Some(_sess) = map.get_mut(&id) {
            //sess.slots[slot as usize] = Some(self.weak_ref.upgrade().unwrap());
        } else {
            // TODO: use max player count
            let slots = vec![None; 7];
            //slots[slot as usize] = Some(self.weak_ref.upgrade().unwrap());
            map.insert(id, Session { id, slots });
        }
    }

    fn handle_packet(&mut self, socket: &UdpSocket, data: &[u8]) -> Result<()> {
        if data[0] == 0x01 {
            // RACE
        } else if data[0] == 0x00 && data[3] == 0x07 && data[4] == 0x02 {
            let pkt = packet::serverbound::ClientSync::parse(data);
            println!("Received sync packet: {:#?}", pkt);

            let time = self.global_clock.elapsed_ms_u16();

            let subpacket = match pkt.subpacket {
                ClientSyncSubpacket::Start {
                    session_id,
                    slot,
                    max_players,
                } => {
                    self.process_sync_start(session_id, slot);
                    SyncSubpacket::Start(clientbound::StartInfo {
                        grid_idx: slot,
                        session_id,
                        slots: SlotBits::from_slot_count(max_players),
                    })
                }
                ClientSyncSubpacket::Sync(val) => SyncSubpacket::Sync(val),
                ClientSyncSubpacket::None => SyncSubpacket::None,
            };

            let pkt = packet::clientbound::ServerSync {
                counter: self.sync_counter,
                time,
                client_hello_time: self.cli_time,
                client_counter: pkt.counter,
                sync_bits: SyncBits::from_count(pkt.counter),
                subpacket,
            };
            self.sync_counter += 1;

            self.send_packet(socket, |wr| {
                pkt.write_to(wr);
            });
        }
        Ok(())
    }

    fn send_packet<F>(&mut self, socket: &UdpSocket, callback: F)
    where
        F: FnOnce(&mut PacketWrite) -> (),
    {
        self.packet_buf.clear();
        let mut cur = Cursor::new(&mut self.packet_buf);
        callback(&mut cur);
        socket.send_to(cur.into_inner(), self.addr);
    }
}

struct Server<'a> {
    clock: GlobalClock,
    clients: HashMap<SocketAddr, Client<'a>>,
    sessions: SessionManager,
}

impl<'a> Server<'a> {
    fn new() -> Self {
        Self {
            clock: GlobalClock::new(),
            clients: HashMap::new(),
            sessions: SessionManager::new(),
        }
    }

    fn run<A>(&'a mut self, addr: A) -> Result<()>
    where
        A: ToSocketAddrs,
    {
        let sock = UdpSocket::bind(addr)?;
        let mut buf = vec![0u8; 4096];
        loop {
            let (len, addr) = sock.recv_from(&mut buf)?;
            println!("Received packet with len {} from {}", len, addr);
            let data = &buf[..len];

            if let Some(cli_time) = parse_hello_packet(data) {
                println!("New client, registering!");
                let mut client = Client::new(&self.clock, &self.sessions, addr, cli_time);
                if let Err(err) = client.respond_hello(&sock) {
                    println!("Failed to send hello: {}", err)
                } else {
                    self.clients.insert(addr, client);
                }
            } else if let Some(client) = self.clients.get_mut(&addr) {
                if let Err(err) = client.handle_packet(&sock, data) {
                    println!("Failed to handle packet: {}", err)
                }
            }
        }
    }
}

fn main() -> Result<()> {
    let mut srv = Server::new();
    srv.run("127.0.0.1:9998")
}

pub fn parse_hello_packet(buf: &[u8]) -> Option<u16> {
    if buf.len() != 75 || buf[0] != 0x00 || buf[3] != 0x06 {
        return None;
    }

    let cli_time = u16::from_be_bytes([buf[69], buf[70]]);
    Some(cli_time)
}
